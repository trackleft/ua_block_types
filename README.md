# UA Block Types

A library of pre-defined fieldable Drupal blocks, based on the [Bean](https://www.drupal.org/project/bean) module, for use in conjunction with the University of Arizona's UA Zen theme.

For a similar approach at a different university, see the [Stanford Bean Types](https://github.com/SU-SWS/stanford_bean_types).

## UA Card

Built to use ua-bootstrap card styles.

## UA Captioned Image

An image wrapped in an HTML5 `figure` with optional attribution and caption information.

## UA Contact Summary

A list of department or unit contact information fields (postal address, email address, phone number) arranged inline; paragraphs within headers or footers are common places to find this kind of summary.

## UA Illustrated Blurb

A paragraph or two of freestanding text, with a heading, decorative or supplementary image and a “Read more” link.

## UA Illustrated Link

A link associated with a decorative or supplementary image.

## UA Mini Blurb

A paragraph or two of freestanding text, with a heading.
